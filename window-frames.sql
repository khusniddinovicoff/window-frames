WITH DailySales AS (
    SELECT
        t.time_id,
        t.day_number_in_week,
        t.calendar_week_number,
        t.day_name,
        SUM(s.amount_sold) AS sales_amount
    FROM
        sh.sales s
        JOIN sh.times t USING (time_id)
    WHERE
        t.calendar_week_number IN (49, 50, 51)
        AND EXTRACT(YEAR FROM t.time_id) = 1999
    GROUP BY
        t.time_id, t.calendar_week_number, t.day_name
),
CumulativeSales AS (
    SELECT
        time_id,
        day_name,
        calendar_week_number,
        sales_amount,
        day_number_in_week,
        SUM(sales_amount) OVER (PARTITION BY calendar_week_number ORDER BY time_id) AS cum_sum
    FROM
        DailySales
),
CenteredAvgSales AS (
    SELECT
        time_id,
        day_name,
        calendar_week_number,
        sales_amount,
        day_number_in_week,
        cum_sum,
        CASE
            WHEN day_number_in_week = 1 THEN
                ROUND(AVG(sales_amount) OVER (ORDER BY time_id RANGE BETWEEN INTERVAL '2' DAY PRECEDING AND INTERVAL '1' DAY FOLLOWING), 2)
            WHEN day_number_in_week = 5 THEN
                ROUND(AVG(sales_amount) OVER (ORDER BY time_id RANGE BETWEEN INTERVAL '1' DAY PRECEDING AND INTERVAL '2' DAY FOLLOWING), 2)
            ELSE
                ROUND(AVG(sales_amount) OVER (ORDER BY time_id RANGE BETWEEN INTERVAL '1' DAY PRECEDING AND INTERVAL '1' DAY FOLLOWING), 2)
        END AS centered_3_day_avg
    FROM
        CumulativeSales
)
SELECT
    time_id,
    day_name,
    calendar_week_number,
    sales_amount,
    cum_sum,
    centered_3_day_avg
FROM
    CenteredAvgSales
ORDER BY
    time_id;

